﻿$(function () {
    // Reference the auto-generated proxy for the hub.
    var filter = $.connection.filterHub;
    // Create a function that the hub can call back to display messages.
    filter.client.getCompanyEmployees = function (model) {
        var users = $.parseJSON(model);

        angular.element($("#angApp")).scope().fillUsersSelector(users);
        angular.element($("#angApp")).scope().$apply();
        

        
       // debugger;



    };

    // Start the connection.
    $.connection.hub.start().done(function () {
        $('#organizationSelect').click(function () {
            angular.element($("#angApp")).scope().userDisable();
            angular.element($("#angApp")).scope().$apply();
        });
        $('#UserId').click(function () {
            angular.element($("#angApp")).scope().competenceDisable();
            angular.element($("#angApp")).scope().$apply();
        });
        $('#organizationSelect').change(function () {
            
            if ($("#organizationSelect option[value='']").length) 
                $("#organizationSelect option[value='']").remove();
            

            filter.server.getUsersDropDownList($("#organizationSelect").val());

            


        });
        $('#UserId').change(function () {
            angular.element($("#angApp")).scope().fillCompetenceSelector();
            angular.element($("#angApp")).scope().$apply();
        });
    });

});