﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Companies.ViewModels
{
    public class OrganizationsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrganizationId { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int EmployeesCount { get; set; }
        public int CompetenciesCount { get; set; }
        public int OfficesCount { get; set; }
        public string ParentCompanyName { get; set; }
        public int? ParentId { get; set; }
        public List<UsersViewModel> Users { get; set; }
        public List<CompetenciesViewModel> Competencies { get; set; }
        public List<OfficesViewModel> Offices { get; set; }

    }
}