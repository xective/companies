﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Companies.ViewModels
{
    public class CompetenciesEvaluationsViewModel
    {
        public int Id { get; set; }
        public int Evaluation { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
        public int CompetenceId { get; set; }
        public string CompetenceName { get; set; }
        public int OrganizationId { get; set; }
    }
}