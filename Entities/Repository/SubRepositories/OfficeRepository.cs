﻿using System;
using System.Collections.Generic;
using System.Linq;
using Companies.Database.Models;

namespace Companies.Database.Repository
{
    public class OfficeRepository : GeneralRepository
    {

        public IQueryable<Office> QueryOffices(int? organizationId)
        {
            var query = this.ListOfTable<Office>();

            if (organizationId > 0)
                return query.Where(o => o.OrganizationId == organizationId).AsQueryable<Office>();

            return null;
        }

        public string GetOfficeName(int? officeId)
        {
            return (officeId != null ? this.GetById<Office>(Convert.ToInt32(officeId)).Name : "");
        }

    }
}