﻿using System;
using System.Linq;
using Companies.Database.Repository;
using Companies.DataManagers;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;

namespace Companies
{
    public class FilterHub : Hub
    {

        public void GetUsersDropDownList(string organizationSelection)
        {
            int organizationSelectionInt = Convert.ToInt32(organizationSelection);



            var Users = new UserDataManager()
                .GetUsersView(organizationSelectionInt)
                .Where(o => o.CompetenceId != null)
                .Select(u => new {
                    Id = u.Id,
                    Name = u.Name,
                    CompetenceId = u.CompetenceId,
                    CompetenceName = u.CompetenceName
                })
                .AsEnumerable();
            var json = JsonConvert.SerializeObject(Users);


            // Call the addNewMessageToPage method to update clients.
            Clients.All.getCompanyEmployees(json);
        }

    }


}