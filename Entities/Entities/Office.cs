﻿using Entities.Entities;

namespace Companies.Database.Models
{
    public class Office : BaseEntity, IBaseOrganizationId
    {
        public string Name { get; set; }
        public string Description { get; set; }


        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
    }
}