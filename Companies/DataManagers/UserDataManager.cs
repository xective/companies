﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Companies.Database.Models;
using Companies.Database.Repository;
using Companies.ViewModels;

namespace Companies.DataManagers
{
    public class UserDataManager : GeneralDataManager
    {
        




        public UsersViewModel GetUserView(int id)
        {
            User UserObject = _userRepository.GetById<User>(id);

            
            return new UsersViewModel
            {
                Id = UserObject.Id,
                Name = UserObject.Name,
                LastName = UserObject.LastName,
                RegistrationDate = UserObject.RegistrationDate,
                Email = UserObject.Email,
                PhoneNumber = UserObject.PhoneNumber,
                OrganizationId = UserObject.OrganizationId,
                OrganizationName = _userRepository.GetById<Organization>(UserObject.OrganizationId).Name,
                OfficeId = UserObject.OfficeId,
                OfficeName = _officeRepository.GetOfficeName(UserObject.OfficeId),
                CompetenceId = UserObject.CompetenceId,
                CompetenceName = _competenceRepository.GetCompetenceName(UserObject.CompetenceId),
                LastEvaluation =  _competenceEvaluationRepository.GetLastEvaluation(UserObject.Id),
                LastEvaluationDate = (_userRepository.ListOfTable<CompetenceEvaluation>().Where(x => x.UserId == UserObject.Id).OrderByDescending(x => x.CreatedDate).FirstOrDefault() != null ?_userRepository.ListOfTable<CompetenceEvaluation>().Where(x => x.UserId == UserObject.Id).OrderByDescending(x => x.CreatedDate).FirstOrDefault().CreatedDate : DateTime.MinValue)

            };
        }


        public List<UsersViewModel> GetUsersView()
        {
            return _userRepository.ListOfTable<User>().Select(o => new UsersViewModel {
                Id = o.Id,
                Name = (o.Name + " " + o.LastName),
                OfficeName = new OfficeRepository().GetOfficeName(o.OfficeId),
                CompetenceName = new CompetenceRepository().GetCompetenceName(o.CompetenceId),
                LastEvaluation = new CompetenceEvaluationRepository().GetLastEvaluation(o.Id)
            }).ToList();
        }
        public List<UsersViewModel> GetUsersView(int organizationId)
        {
            return _userRepository.ListOfTableByOrganization<User>(organizationId).Select(o => new UsersViewModel
            {
                Id = o.Id,
                Name = (o.Name + " " + o.LastName),
                OfficeName = (_userRepository.ListOfTable<Office>().Where(x => x.Id == o.OfficeId).FirstOrDefault() != null ? _userRepository.ListOfTable<Office>().Where(x => x.Id == o.OfficeId).FirstOrDefault().Name : ""),
                CompetenceName = (_userRepository.ListOfTable<Competence>().Where(x => x.Id == o.CompetenceId).FirstOrDefault() != null ? _userRepository.ListOfTable<Competence>().Where(x => x.Id == o.CompetenceId).FirstOrDefault().Name : ""),
                CompetenceId = o.CompetenceId,
                LastEvaluation = (_userRepository.ListOfTable<CompetenceEvaluation>().Where(x => x.UserId == o.Id).OrderByDescending(x => x.CreatedDate).FirstOrDefault() != null ? _userRepository.ListOfTable<CompetenceEvaluation>().Where(x => x.UserId == o.Id).OrderByDescending(x => x.CreatedDate).FirstOrDefault().Evaluation : -1)
            }).ToList();
        }


    }
}