﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Companies.Database;
using Companies.Database.Models;
using Entities.Entities;

namespace Companies.Database.Repository
{
    public class GeneralRepository
    {

        public dynamic DefineTable<T>(CompaniesContext context) where T: BaseEntity
        {
            switch(typeof(T).Name)
            {
                case "CompetenceEvaluation":
                    return context.CompetenciesEvaluations;
                case "Competence":
                    return context.Competencies;
                case "User":
                    return context.Users;
                case "Organization":
                    return context.Organizations;
                case "Office":
                    return context.Offices;
            }
            return null;
        }


        //public IQueryable<User> ListOfTable<User>()
        //{
        //    using (var context = new CompaniesContext())
        //    {
        //        return context.Users.AsQueryable();
        //    }
        //}

        //public IQueryable<Organization> ListOfTable<Organization>()
        //{
        //    using (var context = new CompaniesContext())
        //    {
        //        return context.Organizations.AsQueryable();
        //    }
        //}

        //public IQueryable<Office> ListOfTable<Office>()
        //{
        //    using (var context = new CompaniesContext())
        //    {
        //        return context.Offices.AsQueryable();
        //    }
        //}
        //public IQueryable<Competence> ListOfTable<Competence>()
        //{
        //    using (var context = new CompaniesContext())
        //    {
        //        return context.Competencies.AsQueryable();
        //    }
        //}
        //public IQueryable<CompetenceEvaluation> ListOfTable<CompetenceEvaluation>()
        //{
        //    using (var context = new CompaniesContext())
        //    {
        //        return context.CompetenciesEvaluations.AsQueryable();
        //    }
        //}



        public List<T> ListOfTable<T>() where T : BaseEntity
        {
            return this.ListOfTableParts<T>();
        }

        public List<T> ListOfTableByOrganization<T>(int id) where T : BaseEntity, IBaseOrganizationId
        {
            return ((List<T>)this.ListOfTableParts<T>()).Where(o => o.OrganizationId == id).ToList();
        }

        public dynamic ListOfTableParts<T>() where T : BaseEntity
        {
            using (var context = new CompaniesContext())
            {
                switch (typeof(T).Name)
                {
                    case "CompetenceEvaluation":
                        return context.CompetenciesEvaluations.ToList();
                    case "Competence":
                        return context.Competencies.ToList();
                    case "User":
                        return context.Users.ToList();
                    case "Organization":
                        return context.Organizations.ToList();
                    case "Office":
                        return context.Offices.ToList();
                }
                return null;
            }
        }



        //public IQueryable<T> QueryTable<T>() where T : BaseEntity
        //{
        //    return this.QueryTableParts<T>() as IQueryable<T>;
        //}

        //public dynamic QueryTableParts<T>() where T : BaseEntity
        //{
        //    using (var context = new CompaniesContext())
        //    {
        //        switch (typeof(T).Name)
        //        {
        //            case "CompetenceEvaluation":
        //                return context.CompetenciesEvaluations.AsQueryable();
        //            case "Competence":
        //                return context.Competencies.AsQueryable();
        //            case "User":
        //                return context.Users.AsQueryable();
        //            case "Organization":
        //                return context.Organizations.AsQueryable();
        //            case "Office":
        //                return context.Offices.AsQueryable();
        //        }
        //        return null;
        //    }
        //}








        public T GetById<T>(int id) where T : BaseEntity
        {
            return this.ListOfTable<T>().Where(t => t.Id == id).FirstOrDefault();
        }








        public void Add<T>(T entity) where T : BaseEntity
        {
            using (var context = new CompaniesContext())
            {
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
            }
        }




        public void Update<T>(T entity) where T : BaseEntity
        {
            
            using (var context = new CompaniesContext())
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }




        public void Remove<T>(T entity) where T : BaseEntity
        {
            this.Remove<T>(entity.Id);
        }
        public void Remove<T>(int Id) where T : BaseEntity
        {
            
            using (var context = new CompaniesContext())
            {
                var selectTable = DefineTable<T>(context);
                var entity = selectTable.Find(Id);
                selectTable.Remove(entity);
                context.SaveChanges();
            }
        }

        public void RemoveRange<T>(List<T> entity) where T : BaseEntity
        {
            this.RemoveRange<T>(entity.Select(o => o.Id).ToArray());
        }
        public void RemoveRange<T>(int[] Id) where T : BaseEntity
        {
            using (var context = new CompaniesContext())
            {
                var table = DefineTable<T>(context);
                List<T> Entities = new List<T>();

                foreach (var record in table)
                    if (Queryable.Contains<int>(Id.AsQueryable<int>(), record.Id))
                        Entities.Add(record);

                table.RemoveRange(Entities);
                context.SaveChanges();
            }
        }
        //public void Remove<T>(int id) where T : GeneralRepository
        //{

        //    using (var context = new CompaniesContext())
        //    {
        //        var properties = context.GetType().GetProperties().ToList();
        //        foreach (var property in properties)
        //            if (property.Name == typeof(T).Name)
        //                context.property.ReflectedType

        //        context.Entry().State = EntityState.Deleted;
        //        context.SaveChanges();
        //    }

        //}


        //public T ShallowCopyEntity<T>(T source, CompaniesContext context) where T : BaseEntity
        //{

        //    // Get properties from EF that are read/write and not marked witht he NotMappedAttribute
        //    var sourceProperties = typeof(T)
        //                            .GetProperties()
        //                            .Where(p => p.CanRead && p.CanWrite);
        //    var newObj = new TEntity();

        //    foreach (var property in sourceProperties)
        //    {

        //        // Copy value
        //        property.SetValue(newObj, property.GetValue(source, null), null);

        //    }

        //    return newObj;

        //}

    }
}