﻿using System;
using Entities.Entities;

namespace Companies.Database.Models
{
    public class CompetenceEvaluation : BaseEntity
    {
        public DateTime CreatedDate { get; set; }
        public int Evaluation { get; set; }




        public int CompetenceId { get; set; }
        public virtual Competence Competence { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}