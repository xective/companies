﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Companies.Models;
using Companies.Database.Models;
using Companies.DataManagers;
using Companies.ViewModels;
using Companies.Database.Repository;

namespace Companies.Controllers
{
    public class UserController : Controller
    {
        private OrganizationDataManager _organizationDataManager = null;
        private OfficeDataManager _officeDataManager = null;
        private CompetenceDataManager _competenceDataManager = null;
        private UserDataManager _userDataManager = null;
        private UserRepository _repository = null;

        public UserController()
        {
            _organizationDataManager = new OrganizationDataManager();
            _officeDataManager = new OfficeDataManager();
            _competenceDataManager = new CompetenceDataManager();
            _userDataManager = new UserDataManager();
            _repository = new UserRepository();
        }














        // GET: Employee
        public ActionResult Index()
        {
            return View(_userDataManager.GetUsersView());
        }













        // GET: Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            UsersViewModel model = _userDataManager.GetUserView(id ?? 0);

            if (User == null)
            {
                return HttpNotFound();
            }

            User UserObject = _repository.GetById<User>(id ?? 0);

            if (model.LastEvaluation == -1)
                ViewData["LastEvaluation"] = "employee have not any evaluation";
            else
                ViewData["LastEvaluation"] = model.LastEvaluation + " (" + model.LastEvaluationDate + ")";

            ViewData["OrganizationsList"] = _organizationDataManager.GetAllOrganizationsSelectList(UserObject.OrganizationId);
            ViewData["CompetenciesList"] = _competenceDataManager.GetCompetenciesSelectListByOrganization(UserObject.OrganizationId, UserObject.CompetenceId);
            ViewData["OfficesList"] = _officeDataManager.GetAllOfficesSelectList(UserObject.OrganizationId, UserObject.OfficeId);


            return View(model);
        }
        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "Id,Name,LastName,Email,PhoneNumber,RegistrationDate,OrganizationId,CompetenceId,OfficeId")] User model, string submit)
        {
            //Navigation
            if (submit == "Save")
            {
                
                //Validation
                if (ModelState.IsValid)
                {
                    var User = _repository.GetById<User>(model.Id);


                    //When changes organization office and competence is null.
                    if (model.OrganizationId != User.OrganizationId)
                    {
                        model.CompetenceId = null;
                        model.OfficeId = null;
                    }


                    //Update
                    _repository.Update<User>(model);
                    
                    return RedirectToAction("Index");
                }
                
                return View(model.Id);

            }
            //Navigation
            else
            {
                return RedirectToAction("Index");
            }
        }










        // GET: Employee/Create
        public ActionResult Create()
        {
            ViewData["OrganizationsList"] = _organizationDataManager.GetAllOrganizationsSelectList();
            //ViewData["CompetenciesList"] = _competenceDataManager.GetCompetenciesSelectListByOrganization(UserObject.OrganizationId, UserObject.CompetenceId);
            //ViewData["OfficesList"] = _officeDataManager.GetAllOfficesSelectList(UserObject.OrganizationId, UserObject.OfficeId);
            return View(new UsersViewModel());
        }

        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,LastName,Email,PhoneNumber,RegistrationDate,OrganizationId,CompetenceId,OfficeId")] User User, string submit)
        {
            //Navigation
            if (submit == "Add")
            {
                User.RegistrationDate = DateTime.Now;


                //Validation
                if (ModelState.IsValid)
                {
                    _repository.Add<User>(User);
                    return RedirectToAction("Index");
                }
                
                return View(User);

            }
            //Navigation
            else
            {
                return RedirectToAction("Index");
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
