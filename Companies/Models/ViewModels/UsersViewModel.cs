﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Companies.ViewModels
{
    public class UsersViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string CompetenceName { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public int? OfficeId { get; set; }
        public int? CompetenceId { get; set; }
        public string OfficeName { get; set; }
        public int? LastEvaluation { get; set; }
        public DateTime LastEvaluationDate { get; set; }
    }
}