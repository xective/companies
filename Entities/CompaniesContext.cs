﻿using System.Data.Entity;
using Companies.Database.Models;

namespace Companies.Database
{
    public class CompaniesContext : DbContext
    {
        public CompaniesContext() : base("name=Connection") {}



        public DbSet<Organization> Organizations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Competence> Competencies { get; set; }
        public DbSet<CompetenceEvaluation> CompetenciesEvaluations { get; set; }



    }
}