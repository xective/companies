﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Companies.Models;
using Companies.ViewModels;
using Companies.Database.Models;
using Companies.DataManagers;
using Companies.Database.Repository;

namespace Companies.Controllers
{
    public class CompetenceEvaluationController : Controller
    {
        private OrganizationDataManager _organizationDataManager = null;
        private CompetenceEvaluationDataManager _competenceEvaluationDataManager = null;
        private UserDataManager _userDataManager = null;
        private UserRepository _repository = null;

        public CompetenceEvaluationController()
        {
            _organizationDataManager = new OrganizationDataManager();
            _competenceEvaluationDataManager = new CompetenceEvaluationDataManager();
            _userDataManager = new UserDataManager();
            _repository = new UserRepository();
        }









        // GET: CompetenceEvaluation
        public ActionResult Index()
        {
            return View(_competenceEvaluationDataManager.GetOfficesView());
        }



        // GET: CompetenceEvaluation/Create
        public ActionResult Create()
        {
            var List = _repository.ListOfTable<Organization>().Where(o => Convert.ToBoolean(_repository.ListOfTable<User>().Where(u => (u.CompetenceId != null && u.OrganizationId == o.Id)).ToList().Count)).Select(o => o.Id).ToList();
            ViewData["OrganizationList"] = _organizationDataManager.GetSpecificOrganizationsSelectList(List);
            return View(new CompetenciesEvaluationsViewModel());
        }

        // POST: CompetenceEvaluation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CreatedDate,Evaluation,CompetenceId,UserId")] CompetenciesEvaluationsViewModel competenceEvaluation, string submit)
        {
            //Navigation
            if (submit == "Add")
            {
                //Validation
                var comId = Convert.ToInt32(new UserRepository().GetById<User>(competenceEvaluation.UserId).CompetenceId);

                    var competenceEvaluationUpdate = new CompetenceEvaluation()
                    {
                        Id = competenceEvaluation.Id,
                        CreatedDate = DateTime.Now,
                        Evaluation = competenceEvaluation.Evaluation,
                        CompetenceId = comId,
                        UserId = competenceEvaluation.UserId
                    };

                    _repository.Add<CompetenceEvaluation>(competenceEvaluationUpdate);
                    return RedirectToAction("Index");
                

            }
            else
            {
                return RedirectToAction("Index", "CompetenceEvaluation");
            }

            
        }

















        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}












    }
}
