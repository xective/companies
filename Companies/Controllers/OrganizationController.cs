﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Companies.Models;
using Companies.DataManagers;
using Companies.ViewModels;
using Companies.Database.Repository;
using Companies.Database.Models;

namespace Companies.Controllers
{
    public class OrganizationController : Controller
    {

        
        private OrganizationDataManager _organizationDataManager = null;
        private OrganizationRepository _repository = null;

        public OrganizationController()
        {
            
            _organizationDataManager = new OrganizationDataManager();
            _repository = new OrganizationRepository();

        }


    public ActionResult Index()
        {
            return View(_organizationDataManager.GetOrganizationsView());
        }




        // GET: Organization/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OrganizationsViewModel Organization = new OrganizationDataManager().GetOrganizationView(id ?? 0);

            if (Organization == null)
            {
                return HttpNotFound();
            }

            Organization OrganizationObject = _repository.GetById<Organization>(id ?? 0);

            List<string> evals = new List<string>();

            foreach (var user in Organization.Users)
                if (user.LastEvaluation == -1)
                    evals.Add("");
                else
                    evals.Add(Convert.ToString(user.LastEvaluation));

                ViewData["LastEvaluation"] = evals;


            ViewData["OrganizationsList"] = _organizationDataManager.GetAllOrganizationsSelectList(OrganizationObject.ParentId, id);

            return View(Organization);
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "Id,Name,Description,Status,CreatedDate,ParentId")] Organization Organization, string submit)
        {
            //Navigation
            if (submit == "Save")
            {

                //Validation
                if (ModelState.IsValid)
                {
                    Organization OldOrganization = _repository.GetById<Organization>(Organization.Id);









                    //Check if ParentId changes at all.
                    if (Organization.ParentId != OldOrganization.ParentId)
                    {

                        //If preant exists
                        if (Organization.ParentId != null)
                        {


                            Organization OrganizationParent = _repository.GetById<Organization>(Organization.ParentId ?? 0);

                            if (Organization.Status == "alone")
                                //Make this company hierarchical if it is not.
                                Organization.Status = "hierarchical";



                            
                            if (OrganizationParent.Status == "alone")
                            {

                                OrganizationParent.Status = "hierarchical";
                                _repository.Update<Organization>(OrganizationParent);

                            }



                        }
                        else //Parent changed to NULL.
                        {
                            Organization OldOrganizationParent = _repository.GetById<Organization>(OldOrganization.ParentId ?? 0);

                            //This company
                            Organization.Status = "alone";


                            //Check if parent company should be alone.
                            if (!(Convert.ToBoolean(_repository.ListOfTable<Organization>().Where(o => o.ParentId == OldOrganization.ParentId).Count() - 1)))
                            {
                                OldOrganizationParent.Status = "alone";
                                _repository.Update<Organization>(OldOrganizationParent);
                            }
                                

                        }
                            
                    }







                    _repository.Update<Organization>(Organization);

                    return RedirectToAction("Index", "Organization");
                }

                //If ModelState is not valid
                return View(Organization);


            }
            //Navigation
            else if (submit == "Delete")
            {
                return RedirectToAction("Delete", "Organization", new { id = Organization.Id });
            }
            //Navigation
            else
            {
                return RedirectToAction("Index", "Organization");
            }
        }











        // GET: Organization/Create
        public ActionResult Create()
        {
            //ViewBag.ParentId = new SelectList(new Organization_repository().ListOfTable<Organization>(), "Id", "Name");

            ViewData["OrganizationsList"] = _organizationDataManager.GetAllOrganizationsSelectList();

            return View(new OrganizationsViewModel());
        }

        // POST: Organization/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Status,CreatedDate,ParentId")] Organization Organization, string submit)
        {
            //Navigation
            if (submit == "Add")
            {
                //Validation
                if (ModelState.IsValid)
                {

                    Organization = _organizationDataManager.FormNewEntity(Organization);

                    if (Organization.ParentId != null)
                    {
                        Organization OrganizationParent = _repository.GetById<Organization>(Organization.ParentId ?? 0);

                        if (OrganizationParent.Status == "alone")
                        {
                                //Make parent company hierarchical if it is not.
                                OrganizationParent.Status = "hierarchical";
                                _repository.Update<Organization>(OrganizationParent);
                        }
                    }

                    _repository.Add<Organization>(Organization);
                    return RedirectToAction("Index", "Organization"); ;
                }

                //ViewBag.ParentId = new SelectList(db.Organizations, "Id", "Name", organization.ParentId);
                return View(Organization);



            }
            //Navigation
            else
            {
                return RedirectToAction("Index", "Organization");
            }
        }










        

        // GET: Organization/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OrganizationsViewModel Organization = _organizationDataManager.GetOrganizationView(id ?? 0);



            if (Organization == null)
            {
                return HttpNotFound();
            }
            return View(Organization);
        }

        // POST: Organization/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string submit)
        {
            //Navigation
            if (submit == "Delete")
            {
                var OrganizationChilds = _repository.ListOfTable<Organization>().Where(o => o.ParentId == id).ToList();
                if (OrganizationChilds != null)
                {

                    //Change childs parentid to null and change status if necessary
                    OrganizationChilds.ForEach(o =>
                    {
                        o.ParentId = null;
                        o.Parent = null;

                        //Change status if child have not any childs.
                        if (!Convert.ToBoolean(_repository.ListOfTable<Organization>().Where(x => x.ParentId == o.Id).Count()))
                            o.Status = "Alone";

                        _repository.Update<Organization>(o);
                    });


                }

                //Remove evaluations
                _repository.DeleteOrganization(id);


                return RedirectToAction("Index", "Organization");

            }
            //Navigation
            else
            {
                return RedirectToAction("Details", "Organization", new { id });
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}










    }
}
