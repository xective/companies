﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Companies.Models;
using Companies.Database.Repository;
using Companies.Database.Models;
using Companies.DataManagers;
using Companies.ViewModels;

namespace Companies.Controllers
{
    public class CompetenceController : Controller
    {
        private OrganizationDataManager _organizationDataManager = null;
        private CompetenceDataManager _competenceDataManager = null;
        private CompetenceRepository _repository = null;

        public CompetenceController()
        {
            _organizationDataManager = new OrganizationDataManager();
            _competenceDataManager = new CompetenceDataManager();
            _repository = new CompetenceRepository();
        }















        // GET: Competences
        public ActionResult Index()
        {
            return View(_competenceDataManager.GetCompetenciesView());
        }

















        // GET: Competences/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _competenceDataManager.GetCompetenceView(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            ViewData["OrganizationsList"] = _organizationDataManager.GetAllOrganizationsSelectList(model.OrganizationId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "Id,Name,Description,Status,OrganizationId")] Competence competence, string submit)
        {
            //Navigation
            if (submit == "Save")
            {

                //Validation
                if (ModelState.IsValid)
                {
                    
                        var competenceData = new CompetenceRepository().GetById<Competence>(competence.Id);
                        //If organization changed
                        if (competenceData != null && competenceData.OrganizationId != competence.OrganizationId)
                        {
                            new UserRepository().RemoveUsersCompetence(competence.Id);
                        }

                    _repository.Update<Competence>(competence);

                    return RedirectToAction("Index");
                }


                return View(competence);


            }
            //Navigation
            else
            {
                return RedirectToAction("Index");
            }
        }














        // GET: Competences/Create
        public ActionResult Create()
        {
            ViewData["OrganizationsList"] = _organizationDataManager.GetAllOrganizationsSelectList();
            return View(new CompetenciesViewModel());
        }

        // POST: Competences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Status,OrganizationId")] Competence competence, string submit)
        {
            //Navigation
            if (submit == "Add")
            {
                //Validation
                if (ModelState.IsValid)
                {
                    _repository.Add<Competence>(competence);
                    return RedirectToAction("Index");
                }

                
                return View(competence);



            }
            //Navigation
            else
            {
                return RedirectToAction("Index");
            }
        }

        











        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}


        



    }
}
