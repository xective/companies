﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Companies.Database.Models;
using Companies.Database.Repository;
using Companies.ViewModels;

namespace Companies.DataManagers
{
    public class GeneralDataManager
    {
        protected UserRepository _userRepository = null;
        protected OfficeRepository _officeRepository = null;
        protected OrganizationRepository _organizationRepository = null;
        protected CompetenceRepository _competenceRepository = null;
        protected CompetenceEvaluationRepository _competenceEvaluationRepository = null;
        public GeneralDataManager()
        {
            _userRepository = new UserRepository();
            _officeRepository = new OfficeRepository();
            _organizationRepository = new OrganizationRepository();
            _competenceRepository = new CompetenceRepository();
            _competenceEvaluationRepository = new CompetenceEvaluationRepository();

        }



        //public string ParseToString(string value)
        //{
        //    return (value == null ? "" : value);
        //}


    }
}
