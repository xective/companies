﻿using System;
using System.Collections.Generic;
using Entities.Entities;

namespace Companies.Database.Models
{
    public class User : BaseEntity, IBaseOrganizationId
    {

        public User()
        {
            CompetenceEvaluations = new List<CompetenceEvaluation>();
        }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime RegistrationDate { get; set; }





        public virtual List<CompetenceEvaluation> CompetenceEvaluations { get; set; }
        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
        public int? OfficeId { get; set; }
        public int? CompetenceId { get; set; }
    }
}