﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Companies.Database.Models;
using Companies.Database.Repository;
using Companies.ViewModels;

namespace Companies.DataManagers
{
    public class OfficeDataManager : GeneralDataManager
    {

       



        public OfficesViewModel GetOfficeView(int? id)
        {
            Office OfficeObject = _officeRepository.GetById<Office>(id ?? 0);

            return new OfficesViewModel()
            {
                Id = OfficeObject.Id,
                Name = OfficeObject.Name,
                Description = OfficeObject.Description,
                OrganizationId = OfficeObject.OrganizationId,
                OrganizationName = _officeRepository.ListOfTable<Organization>().Where(x => x.Id == OfficeObject.OrganizationId).FirstOrDefault().Name
            };

        }




        public List<OfficesViewModel> GetOfficesView()
        {
            return _officeRepository.ListOfTable<Office>().Select(o => new OfficesViewModel()
            {
                Id = o.Id,
                Name = o.Name,
                Description = o.Description,
                OrganizationId = o.OrganizationId,
                OrganizationName = _officeRepository.ListOfTable<Organization>().Where(x => x.Id == o.OrganizationId).FirstOrDefault().Name
            }).ToList();

        }

        public List<OfficesViewModel> GetOfficesView(int organizationId)
        {
            return _officeRepository.ListOfTableByOrganization<Office>(organizationId).Select(o => new OfficesViewModel()
            {
                Id = o.Id,
                Name = o.Name,
                Description = o.Description,
                OrganizationId = o.OrganizationId,
                OrganizationName = _officeRepository.ListOfTable<Organization>().Where(x => x.Id == o.OrganizationId).FirstOrDefault().Name
            }).ToList();

        }











        //public IEnumerable<SelectListItem> GetAllOfficesSelectList(int? selected, int? ExceptThisId = null)
        //{
        //    if (Convert.ToBoolean(ExceptThisId))
        //        return Repository.ListOfTable<Office>().Where(o => o.Id != ExceptThisId).Select(o => new SelectListItem
        //        {
        //            Text = o.Name,
        //            Value = o.Id.ToString(),
        //            Selected = (selected == o.Id ? true : false)
        //        }).AsEnumerable();
        //    else
        //        return Repository.ListOfTable<Office>().Select(o => new SelectListItem
        //        {
        //            Text = o.Name,
        //            Value = o.Id.ToString(),
        //            Selected = (selected == o.Id ? true : false)
        //        }).AsEnumerable();

        //}

        public IEnumerable<SelectListItem> GetAllOfficesSelectList(int organizationId, int? selected = null)
        {
            return _officeRepository
                .ListOfTableByOrganization<Office>(organizationId)
                .Select(o => new SelectListItem
                {
                    Text = o.Name,
                    Value = o.Id.ToString(),
                    Selected = (selected == o.Id ? true : false)
                })
                .AsEnumerable();

        }



    }
}