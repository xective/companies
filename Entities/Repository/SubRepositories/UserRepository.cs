﻿using System;
using System.Collections.Generic;
using System.Linq;
using Companies.Database.Models;
using EntityFramework.Extensions;

namespace Companies.Database.Repository
{
    public class UserRepository : GeneralRepository
    {


        public IQueryable<User> QueryUsers(int? organizationId)
        {
            var query = this.ListOfTable<User>();

            if (organizationId > 0)
                return query.Where(o => o.OrganizationId == organizationId).AsQueryable<User>();

            return null;
        }

        public void RemoveUsersCompetence(int competentionId)
        {
            var users = this.ListOfTable<User>().Where(t => t.CompetenceId == competentionId).ToList();
            users.ForEach(o => o.CompetenceId = null);
            //var test = users.Update(t => new User { CompetenceId = null });
            foreach (var user in users)
            {
                this.Update<User>(user);
            }
            
        }

        public void DeleteUsersById(int[] userIds)
        {
            var users = this.ListOfTable<User>().Where(t => userIds.Contains(t.Id)).ToList();
            this.RemoveRange<User>(users);
        }



    }
}