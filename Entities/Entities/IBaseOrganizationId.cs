﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Companies.Database.Models;

namespace Entities.Entities
{
    public interface IBaseOrganizationId
    {
        int OrganizationId { get; set; }
        Organization Organization { get; set; }
    }
}
