﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Companies.Database.Models;
using Companies.Database.Repository;
using Companies.ViewModels;

namespace Companies.DataManagers
{
    public class OrganizationDataManager : GeneralDataManager
    {
       





        public List<OrganizationsViewModel> GetOrganizationsView()
        {


            return _organizationRepository.ListOfTable<Organization>()
                    .Select(o => new OrganizationsViewModel()
                    {
                        Id = o.Id,
                        Name = o.Name,
                        OfficesCount = _organizationRepository.ListOfTable<Office>().Where(x => x.OrganizationId == o.Id).Count(),
                        EmployeesCount = _organizationRepository.ListOfTable<User>().Where(x => x.OrganizationId == o.Id).Count(),
                        CompetenciesCount = _organizationRepository.ListOfTable<Competence>().Where(x => x.OrganizationId == o.Id).Count()
                    }).ToList();
        }






        public OrganizationsViewModel GetOrganizationView(int id)
        {

            var RequestedOrganization = _organizationRepository.GetById<Organization>(id);


            return new OrganizationsViewModel()
            {
                OrganizationId = RequestedOrganization.Id,
                Name = RequestedOrganization.Name,
                Description = RequestedOrganization.Description,
                Status = RequestedOrganization.Status,
                CreatedDate = RequestedOrganization.CreatedDate,
                ParentId = RequestedOrganization.ParentId,
                ParentCompanyName = (_organizationRepository.GetById<Organization>(RequestedOrganization.ParentId ?? 0) != null ? _organizationRepository.GetById<Organization>(RequestedOrganization.ParentId ?? 0).Name : ""),
                Users = new UserDataManager().GetUsersView(RequestedOrganization.Id),
                Competencies = new CompetenceDataManager().GetCompetenciesViewByOrganization(RequestedOrganization.Id),
                Offices = new OfficeDataManager().GetOfficesView(RequestedOrganization.Id)
            };


        }




        public Organization FormNewEntity(Organization Organization)
        {
            return Organization = new Organization
            {
                Id = Organization.Id,
                Name = Organization.Name,
                Description = Organization.Description,
                Status = Organization.ParentId == null ? "alone" : "hierarchical",
                CreatedDate = DateTime.Now,
                ParentId = Organization.ParentId
            };
        }





































        public IEnumerable<SelectListItem> GetAllOrganizationsSelectList(int? selectedId = null, int? ExceptThisId = null)
        {
            return _organizationRepository.ListOfTable<Organization>().Where(o => o.Id != ExceptThisId).Select(o => new SelectListItem
            {
                Text = o.Name,
                Value = o.Id.ToString(),
                Selected = (selectedId == o.Id ? true : false)
                //(OrganizationsQuery.Where(t => t.Id == ExceptThisId).Select(t => t.ParentId).FirstOrDefault() == o.Id ? true : false)
            }).AsEnumerable();
        }


        public IEnumerable<SelectListItem> GetSpecificOrganizationsSelectList(List<int> ItemsId, int? selectedId = null)
        {
            return _organizationRepository.ListOfTable<Organization>().Where(o => ItemsId.Contains(o.Id)).Select(o => new SelectListItem
            {
                Text = o.Name,
                Value = o.Id.ToString(),
                Selected = (selectedId == o.Id ? true : false)
                //(OrganizationsQuery.Where(t => t.Id == ExceptThisId).Select(t => t.ParentId).FirstOrDefault() == o.Id ? true : false)
            }).AsEnumerable();
        }
















    }
}