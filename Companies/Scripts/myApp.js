﻿(function () {

    var myApp = angular.module('myApp', ['ngRoute']);

    myApp.controller('MainController', ['$scope', function ($scope) {
        
        //Disabled by default
        $scope.userDis = true;
        $scope.competenceDis = true;
        $scope.evalDis = true;
        $scope.usersArray = [];


        $scope.userDisable = function () {
            $scope.userDis = false;
            $scope.userSelected= "";
            $scope.eval = "";
            $scope.competence = "";
            $scope.valCompetenceId = "";
            //$scope.evalDis = true;
        };

        //$scope.deleteEmpty = function () {
        //    $scope.eval = "";
        //    if ($("#UserId").val() == "")
        //        $scope.evalDis = true;
        //    else
        //        $scope.evalDis = false;

        //};

        $scope.competenceDisable = function () {
            $scope.eval = "";
            if ($("#UserId").val() == "")
                $scope.evalDis = true;
            else
                $scope.evalDis = false;
            
        };

        $scope.fillUsersSelector = function (users) {
            $scope.usersArray = users;
            $scope.userDis = false;
        };

        $scope.fillCompetenceSelector = function () {
            angular.forEach($scope.usersArray, function (value, key) {
                if (value.Id == $("#UserId").val()){
                    $scope.competence = value.CompetenceName;
                    $scope.valCompetenceId = value.CompetenceId
                }
            });
            
        };




    }]);














})();