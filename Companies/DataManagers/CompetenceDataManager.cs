﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Companies.Database.Models;
using Companies.Database.Repository;
using Companies.ViewModels;

namespace Companies.DataManagers
{
    public class CompetenceDataManager : GeneralDataManager
    {







        public CompetenciesViewModel GetCompetenceView(int? id)
        {
            Competence CompetenceObject = _competenceRepository.GetById<Competence>(id ?? 0);

            return new CompetenciesViewModel()
            {
                Id = CompetenceObject.Id,
                Name = CompetenceObject.Name,
                Description = CompetenceObject.Description,
                Status = CompetenceObject.Status,
                OrganizationId = CompetenceObject.OrganizationId,
                OrganizationName = _competenceRepository.ListOfTable<Organization>().Where(x => x.Id == CompetenceObject.OrganizationId).FirstOrDefault().Name
            };

        }




        public List<CompetenciesViewModel> GetCompetenciesView()
        {
            return _competenceRepository.ListOfTable<Competence>().Select(o => new CompetenciesViewModel()
            {
                Id = o.Id,
                Name = o.Name,
                Description = o.Description,
                Status = o.Status,
                OrganizationId = o.OrganizationId,
                OrganizationName = _competenceRepository.ListOfTable<Organization>().Where(x => x.Id == o.OrganizationId).FirstOrDefault().Name
            }).ToList();

        }

        public List<CompetenciesViewModel> GetCompetenciesViewByOrganization(int organizationId)
        {
            return _competenceRepository.ListOfTableByOrganization<Competence>(organizationId).Select(o => new CompetenciesViewModel()
            {
                Id = o.Id,
                Name = o.Name,
                Description = o.Description,
                Status = o.Status,
                OrganizationId = o.OrganizationId,
                OrganizationName = _competenceRepository.ListOfTable<Organization>().Where(x => x.Id == o.OrganizationId).FirstOrDefault().Name
            }).ToList();

        }


























        public IEnumerable<SelectListItem> GetCompetenciesSelectListByOrganization(int organizationId, int? selected = null)
        {
                return new CompetenceRepository()
                    .ListOfTableByOrganization<Competence>(organizationId)
                    .Select(o => new SelectListItem
                     {
                         Text = o.Name,
                         Value = o.Id.ToString(),
                         Selected = (selected == o.Id ? true : false)
                     })
                    .AsEnumerable();

        }


    }
}