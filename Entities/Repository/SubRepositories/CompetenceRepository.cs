﻿using System;
using System.Collections.Generic;
using System.Linq;
using Companies.Database.Repository;
using Companies.Database.Models;

namespace Companies.Database.Repository
{
    public class CompetenceRepository : GeneralRepository
    {

        public IQueryable<Competence> QueryCompetencies(int? organizationId)
        {
            var query = this.ListOfTable<Competence>();

            if(organizationId > 0)
                return query.Where(o => o.OrganizationId == organizationId).AsQueryable<Competence>();

            return null;
        }

        public string GetCompetenceName(int? competenceId)
        {
            return (competenceId != null ? this.GetById<Competence>(Convert.ToInt32(competenceId)).Name : "");
        }

    }
}