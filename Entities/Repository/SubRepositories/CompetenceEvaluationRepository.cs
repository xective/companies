﻿using System.Linq;
using Companies.Database.Repository;
using Companies.Database.Models;
using System;

namespace Companies.Database.Repository
{
    public class CompetenceEvaluationRepository : GeneralRepository
    {


        public int? GetLastEvaluation(int userId)    
        {
            var checker = ListOfTable<CompetenceEvaluation>().Where(x => x.UserId == userId).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
            return (checker != null ? (int?)checker.Evaluation  : null);
        }

    }
}