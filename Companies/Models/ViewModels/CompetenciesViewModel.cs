﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Companies.ViewModels
{
    public class CompetenciesViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public int LastEvaluation { get; set; }
    }
}