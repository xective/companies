namespace Companies.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Database;

    internal sealed class Configuration : DbMigrationsConfiguration<CompaniesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CompanyDb";
        }

        protected override void Seed(CompaniesContext context)
        {

        }
    }
}
