﻿using System;
using System.Collections.Generic;
using Entities.Entities;

namespace Companies.Database.Models
{
    public class Organization : BaseEntity
    {
        public Organization()
        {
            Users = new List<User>();
            Competencies = new List<Competence>();
            Offices = new List<Office>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }






        public virtual List<User> Users { get; set; }
        public virtual List<Competence> Competencies { get; set; }
        public virtual List<Office> Offices { get; set; }
        public int? ParentId { get; set; }
        public virtual Organization Parent { get; set; }

    }
}