﻿using System.Collections.Generic;
using Entities.Entities;

namespace Companies.Database.Models
{
    public class Competence : BaseEntity, IBaseOrganizationId
    {
        public Competence()
        {
            Evaluations = new List<CompetenceEvaluation>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }




        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual List<CompetenceEvaluation> Evaluations { get; set; }

    }
}