﻿using System.Linq;
using Companies.Database.Models;
using System;
using System.Collections.Generic;

namespace Companies.Database.Repository
{
    public class OrganizationRepository : GeneralRepository
    {
        public void DeleteOrganization(int organizationId)
        {
            var orgUsers = this.GetUsersByOrganization(organizationId);

            

            foreach (var user in orgUsers)
            {
                //Firstly delete competence evaluations
                var compEval = this.ListOfTable<CompetenceEvaluation>().Where(o => o.UserId == user.Id).ToList();
                if (compEval != null && compEval.Count != 0)
                    this.RemoveRange<CompetenceEvaluation>(compEval);

                //Delete competence
                if (user.CompetenceId != null)
                    this.Remove<Competence>((int)user.CompetenceId);

                //Delete office
                if (user.OfficeId != null)
                    this.Remove<Office>((int)user.OfficeId);

                //Delete User
                this.Remove<User>(user);

            }

            //Delete organization data which is not associated with users
            //Frist competencies
            var competencies = this.ListOfTable<Competence>().Where(o => o.OrganizationId == organizationId).ToList();
            if (competencies != null && competencies.Count != 0)
                this.RemoveRange<Competence>(competencies);

            //Second offices
            var offices = this.ListOfTable<Office>().Where(o => o.OrganizationId == organizationId).ToList();
            if (offices != null && offices.Count != 0)
                this.RemoveRange<Office>(offices);




            //Delete Organization
            this.Remove<Organization>(organizationId);
            
        }

        public List<User> GetUsersByOrganization(int organizationId)
        {
            return this.ListOfTable<User>().Where(o => o.OrganizationId == organizationId).ToList();
        }

    }
}