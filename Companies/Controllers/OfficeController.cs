﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Companies.Models;
using Companies.DataManagers;
using Companies.ViewModels;
using Companies.Database.Models;
using Companies.Database.Repository;

namespace Companies.Controllers
{
    public class OfficeController : Controller
    {
        private OrganizationDataManager _organizationDataManager = null;
        private OfficeDataManager _officeDataManager = null;
        private OfficeRepository _repository = null;

        public OfficeController()
        {
            _organizationDataManager = new OrganizationDataManager();
            _officeDataManager = new OfficeDataManager();
            _repository = new OfficeRepository();
        }










        // GET: Offices
        public ActionResult Index()
        {
            return View(_officeDataManager.GetOfficesView());
        }












        // GET: Offices/Create
        public ActionResult Create()
        {
            ViewData["OrganizationsList"] = _organizationDataManager.GetAllOrganizationsSelectList();
            return View(new OfficesViewModel());
        }

        // POST: Offices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,OrganizationId")] Office office, string submit)
        {
            //Navigation
            if (submit == "Add")
            {

            //Validation
            if (ModelState.IsValid)
            {
                _repository.Add<Office>(office);
                return RedirectToAction("Index");
            }



            return View(office);


            }
            //Navigation
            else
            {
                return RedirectToAction("Index");
            }
        }

        






        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}















    }
}
