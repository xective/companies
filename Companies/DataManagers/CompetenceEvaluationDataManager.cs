﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Companies.Database.Models;
using Companies.Database.Repository;
using Companies.ViewModels;

namespace Companies.DataManagers
{
    public class CompetenceEvaluationDataManager : GeneralDataManager
    {

        


        public List<CompetenciesEvaluationsViewModel> GetOfficesView()
        {

            return _competenceEvaluationRepository.ListOfTable<CompetenceEvaluation>().Select(o => new CompetenciesEvaluationsViewModel()
            {
                Id = o.Id,
                Evaluation = o.Evaluation,
                CreatedDate = o.CreatedDate,
                UserId = o.UserId,
                UserFullName = (_competenceEvaluationRepository.ListOfTable<User>().Where(x => x.Id == o.UserId).FirstOrDefault().Name + " " + _competenceEvaluationRepository.ListOfTable<User>().Where(x => x.Id == o.UserId).FirstOrDefault().LastName),
                CompetenceId = o.CompetenceId,
                CompetenceName = _competenceEvaluationRepository.ListOfTable<Competence>().Where(x => x.Id == o.CompetenceId).FirstOrDefault().Name
        }).ToList();

        }


    }
}